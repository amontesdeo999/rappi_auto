import BuyProduct from '../support/pages/buy_product'
import SelectLocation from '../support/pages/select_location'

describe("Buy Products", function () {

    const selectLocation = new SelectLocation()
    const buyProduct = new BuyProduct()
    const shoppingList = ["Manzana Red", "Kin Agua Mineral Bidon", "Casancrem Queso Crema Entero", "Ledesma Azúcar Clásica"]



    beforeEach(function () {
        selectLocation.visit()
        selectLocation.select()
    })

    afterEach(function () {
        cy.wait(3000)
        cy.xpath("/html[1]/body[1]/div[1]/rappi-mf-header[1]/div[1]/div[1]/div[2]/div[2]/div[1]/button[1]/span[1]").click()
        cy.contains('span', 'Vaciar todas las canastas').click()
        cy.contains('span', 'Sí, seguro').click()
    })


    it('Buy Category Supermercados', function () {


        buyProduct.buyProductCategorySuper(shoppingList)
    })




})