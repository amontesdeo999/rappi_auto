class SearchProduct {


    visit() {
        cy.visit("/")
    }

    search(text) {

        cy.wait(5000)
        cy.get('[data-testid=search_input]').click()
        cy.wait(2000)
        cy.get('[data-testid=search_input]').type(text).type("{enter}")
        cy.wait(5000)
        cy.get('.styles__EmptyList-sc-w9k2vc-8').should('not.exist');

    }
}

export default SearchProduct