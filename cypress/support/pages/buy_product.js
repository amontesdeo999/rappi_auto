class BuyProduct {


    visit() {
        cy.visit("/")
    }

    buyProductCategorySuper(shoppingList) {

        cy.wait(5000)

        cy.get('[data-index="1"] > :nth-child(1) > .category_slider_item > .styles__CategoryContainer-sc-1sbgiz8-0 > .styles__CategoryImage-sc-1sbgiz8-1').click()

        cy.get(':nth-child(2) > .styles__SliderSection-sc-1fpso54-7 > .swiper-container > .swiper-wrapper > .swiper-slide-active > a > .styles__StoreCardContainer-sc-j5j8go-0 > .Storelogostyles__StoreLogoContainer-sc-icjfwj-0 > [style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0"] > [data-testid=image]').click()

        cy.wait(3000)
        
        for(var i=0;i<shoppingList.length;i++){

            cy.get('[data-testid="FieldDefault"').type(shoppingList[i]).type("{enter}")
            cy.contains('h3', shoppingList[i]).click();
            cy.wait(4000)
            cy.contains('span', 'Agregar').click()
            cy.wait(3000)
            cy.get('#CancelButton').click()
        
        }

   
        cy.xpath("/html[1]/body[1]/div[1]/rappi-mf-header[1]/div[1]/div[1]/div[2]/div[2]/div[1]/button[1]/span[1]").click()
        cy.xpath("/html[1]/body[1]/div[1]/custom-micro-basket[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[2]/div[2]").click()
        cy.get('.ArrowBackStyled-sc-17z2wjg').click()





    }
}

export default BuyProduct